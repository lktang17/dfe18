package com.qa.beans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class BeanDemoApp {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(BeanDemoApp.class, args);
		
//		
//		    Object byName = context.getBean("timenow");
//		    String byType = context.getBean(String.class);
//		    String byBoth = context.getBean("timenow", String.class);
//
//		    System.out.println("byName" + " " + byName);
//		    System.out.println("byType" + " " + byType);
//		    System.out.println("byBoth" + " " + byBoth);

		// get the bean from the spring container singleton
		Object SingletonbyName1 = context.getBean("greeting", String.class);
		Object SingletonbyName2 = context.getBean("greeting", String.class);
		
		// call a method on the bean
		
	    System.out.println("singleton" + " " + SingletonbyName1);
	    System.out.println("Are the two beans same :" + (SingletonbyName1==SingletonbyName2));
	    System.out.println("s1  " + SingletonbyName1.hashCode());
	    System.out.println("s2: " + SingletonbyName2.hashCode());

	    
	    
		/// get the bean from the spring container  prototype
		Object ProtoTypebyName1 = context.getBean("farewell",  String.class);
				
		// call a method on the bean
		System.out.println("prototype" + " "  + ProtoTypebyName1);
		
		
		
		
		
		
	}

}
