package com.qa.demo.persistence.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Account {
	
	
	//Fields 

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long  id ;
	
    @Column(unique = true, nullable = false)
	private long accountNumber;
	
	private String name;
	
	private int age;

	
	//constructors
	public Account() {
		
	}
	
	
	public Account(long id,long accountNumber, String name) {
		super();
		this.id = id;
		this.accountNumber = accountNumber;
		this.name = name;
	}

	//getters and setters
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

}
