package com.qa.demo.rest;
import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.qa.demo.persistence.domain.Account;
import com.qa.demo.services.AccountService;



	@RestController	
	public class AccountController{
		
		//refer to the service api 
		private AccountService service;

	    public AccountController(AccountService service) 
	    
	    {
	        super();
	        this.service = service;
	    }

		
		
	private List<Account> account = new ArrayList<>();

		//test
	    @GetMapping("/test")
	    public String test() {
	        return "Hello, World! Accounts";
	    } 	
	    
	    //start calls to crud in the SERVICE layer
	    
	    // Create
	    @PostMapping("/create")
	    public Account addAccount (@RequestBody Account account) {
	        return this.service.addAccount (account);
	    }
	    
	    
	    @GetMapping("/getAll")
	    public List<Account> getAllPeople() {
	        return this.service.getAllAccount();
	    }
	    
	    
	  //update
	    @PutMapping("/update")
	    public Account updateAccount(@PathParam("id") int id, @RequestBody Account account ) {
	        return this.service.updateAccount(id, account);
	    }
	    
	  //delete
	    @DeleteMapping("/delete/{id}")
	    public Account removeAccount (@PathVariable int id) {
	        return this.service.removeAccount(id);
	    }
	}

	
	
	
	


