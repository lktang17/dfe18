package com.qa.rest;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.qa.services.PersonService;

@RestController	
public class PersonController {
	
	  private PersonService service;

	    public PersonController(PersonService service) 
	    
	    {
	        super();
	        this.service = service;
	    }

//private List<Person> people = new ArrayList<>();

//crud starts

    @GetMapping("/test")
    public String test() {
        return "Hello, World! from Person";
    } 	
    
    
    
    // Create
    @PostMapping("/create")
    public Person addPerson(@RequestBody Person person) {
        return this.service.addPerson(person);
    }
    
    

    
    @GetMapping("/getAll")
    public List<Person> getAllPeople() {
        return this.service.getAllPeople();
    }
    
    
    //update
    @PutMapping("/update")
    public Person updatePerson(@PathParam("id") Long id, @RequestBody Person person) {
        return this.service.updatePerson(id, person);
    }

    
    
  //delete
    @DeleteMapping("/delete/{id}")
    public boolean removePerson(@PathVariable Long id) {
        return this.service.removePerson(id);
    }
}
