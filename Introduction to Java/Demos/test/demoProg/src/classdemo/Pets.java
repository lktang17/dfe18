package classdemo;

public class Pets {

	@Override
	public String toString() {
		return "Pets [age=" + age + ", breed=" + breed + ", colour=" + colour + ", name=" + name + "]";
	}

	private String name;
	private float age;
	private String breed;
	private String colour;

	// constructor

	public Pets() {
		this.name = name;
		this.age = age;
		this.breed = breed;
		this.colour = colour;
	}

	public Pets(String name) {
		this.name = name;
	}

	// add methods

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getAge() {
		return age;
	}

	public void setAge(float age) {
		this.age = age;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

}
