package booksChallenge;

public class TestBooks {
	
	public static void main(String[] args) {
		String[] arr = {"Author1", "Author2", "Author3"};
		
		Book b = new Book("title", arr,  10.99f);
		Book b2 = new Book("title2", arr, 5.99f);
		Book b3 = new Book("title3", arr, 1.00f);
		
		//6
		System.out.println(b);
		
		//7
		Book[] bookArray = new Book[3];
		bookArray[0] = b;
		bookArray[1] = b2;
		bookArray[2] = b3;
		//this could have been done with  "Book[] bookArray = {b, b2, b3};"
		
		for (int x = 0; x < bookArray.length; x++){
			System.out.println(bookArray[x]);
		}
	}

}