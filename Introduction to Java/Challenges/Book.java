package booksChallenge;


public class Book{

	
	

	//add fields
	private String name;
	private String[] authors;
	private float price; 
	 
	
	/**
	 *  add Constructor for book
	 * @param name String title of the book
	 * @param authors String array up to five authors
	 * @param d double price of the book
	 */
	
	public Book(String name, String[] authors, float d) {
		this.name = name;
		this.authors = authors;
		this.price = d;
	}

	// add getter/setter or methods
	
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	/**
	 * Set the authors of the book, replaced the old array entirely
	 * Will only accept up to 5 authors in the array, otherwise an 
	 * error is thrown and no change is made
	 * 
	 * @param authors array of authors to replace the current set
	 */
	

	

	
	public float getPrice() {
		return price;
	}


	public void setPrice(float price) {
		this.price = price;
	}


	public String[]addAuthor() {
		
		//add code to add authors to the book !
		return authors;
	}
	
	
	/**
	 * Add an individual author to the list of authors of the book
	 * Will print an error if there are already 5 listed authors
	 * @param author the author to add to the array
	 */
	public void addAuthor(String author) {
		if (authors.length <= 5) {
			
			authors[authors.length] = author;
			
		} else {
			System.out.println("Error, you can only have up to five authors. Change not made");
		}
	}

	/**
	 * add toString method for the class, returns the object in the form
	 * Book: [Title, Author1, Author2, .... , Price]
	 */
	
	
	//5
		public String toString(){
			String str = "Book: [" + name + ", ";
			
			for(int x = 0; x < authors.length; x ++){
				str += authors[x] + ", ";
			}
			
			str += price + "]";
			
			return str;
	}
	
}
