package crudDemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySqlConnectionTest {
    public static void main(String[] args) {
 
        // creates three different Connection objects
        Connection conn1 = null;
      
        try {
            // connect to database
            String url1 = "jdbc:mysql://localhost:3306/testdb";
            String user = "root";
            String password = "Pa$$w0rd";
 
            conn1 = DriverManager.getConnection(url1, user, password);
            if (conn1 != null) {
                System.out.println("Connected to the database testDb");
                
             //add code to read from database
                

          	  	String sql = "SELECT * FROM employees";
                
                Statement statement = conn1.createStatement();
                ResultSet result = statement.executeQuery(sql);
                

                
                while (result.next()){

                String name = result.getString("Name");
                int age = result.getInt("Age");
                String dept = result.getString("Dept");
              
               
                System.out.println(name + " " + age + " " + dept);
                }

                
               //add code to update the  database
                
                String sql1 = "INSERT INTO employees(  name, age, dept) VALUES ( ?, ?, ?)";
                
//                PreparedStatement statement1 = conn1.prepareStatement(sql1);
//                
//                statement1.setString(1, "bill");
//                statement1.setInt(2, 66);
//                statement1.setString(3, "IT");
//
//                 
//                int rowsInserted = statement1.executeUpdate();
//                if (rowsInserted > 0) {
//                    System.out.println("A new employee was inserted successfully!");
//                }
                
              //add code to update employee in the employees database

                String sql2 = "UPDATE employees SET  age=?, dept=?, idEmployees=?  WHERE name=?";

                PreparedStatement statement2 = conn1.prepareStatement(sql2);
    			statement2.setString(1, "Mon");
    			statement2.setInt(2,77);
    			statement2.setString(3, "Training");
    			statement2.setString(4, "peter");
    			
                 
                int rowsUpdated = statement2.executeUpdate();
                if (rowsUpdated > 0) {
                    System.out.println("An existing employee was updated successfully!");
                }

                
        }
 
        } catch (SQLException ex) {
            System.out.println("An error occurred. Maybe user/password is invalid");
            ex.printStackTrace();
        }
      
        
     
        
        
        
    }
}



































